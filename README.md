## Rake Algorithm

## Bài 2: Extract Keyword từ văn bản

## Đề bài:
- sử dụng thuật toán [Rake](https://www.analyticsvidhya.com/blog/2021/10/rapid-keyword-extraction-rake-algorithm-in-natural-language-processing/) extract word từ [văn bản](https://trello.com/1/cards/630880ef8b5e70010e975c87/attachments/63088a97c51e730197fbd814/download/text.txt)
- sử dụng bộ [stopword.json](https://trello.com/1/cards/630880ef8b5e70010e975c87/attachments/63088a96dab43a015a863f37/download/stopword.json) 

## Thuật toán:
- Rake (Rapid Automatic Keyword Extraction) là một thuật toán trích xuất từ khóa để tìm từ và cụm từ có liên quan nhất trong văn bản
- Input: 
    + paragraph
    + stopword
- Output:
    + Cumulative score of Candidate Key Phrases
- Note:
    + Corpus[]: tất cả các từ đơn trong đọan văn
    + StopWord[]: stopword
    + Delimited[]: các dấu câu
    + Content_word = Corpus - StopWord - Delimited : các từ đơn có nghĩa
    + Candidate_Key_Phrases[] : các cụm từ khóa ứng viên
    + Degree score = Degree of word / Word frequency
- Ý tưởng thuật toán: 
    + Từ đoạn văn bản ban đầu, ta tách đoạn văn thành các câu văn đơn sử dụng regex, dựa vào các dấu câu => Mảng các câu văn đơn
    + Từ mảng các câu văn trên ta xét từng câu văn để lấy ra các cụm từ khóa, sử dụng regex, dựa vào mảng stopword => Mảng các cụm từ khóa
    + Xác định Word frequency, Degree of word, Degree score cho từng từ đơn (Degree score = Degree of word / Word frequency)
    + Tính điểm tích lũy cho từng cụm từ (Cumulative score of Candidate Key Phrases) 

## Link tham khảo
- Rake_Algoritms: https://www.analyticsvidhya.com/blog/2021/10/rapid-keyword-extraction-rake-algorithm-in-natural-language-processing/
