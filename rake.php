<?php

require "src/Rake_Algorithm.php";

use Rake_Algorithm\Rake_Algorithm;

$str = file_get_contents(__DIR__ . '/asset/text_vi.txt');
$rake = new Rake_Algorithm($str);
$keyWords = $rake->getKeyword(15);

$result = '';
foreach ($keyWords as $keyWord => $score) {
    $result .= $keyWord . ' ==> ' . $score . "\n\r";
}

print_r($result);

